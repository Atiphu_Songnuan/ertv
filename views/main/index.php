<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" >
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ERTV</title>
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
    <style>
        html,body {
            height: 100%;
        }

        @font-face {
            font-family: niramit;
            src: url(public/fonts/niramit/TH-Niramit-AS.ttf);
        }

        @font-face {
            font-family: niramit-bold;
            src: url(public/fonts/niramit/TH-Niramit-AS-Bold.ttf);
        }

        div {
            font-family: niramit;
        }
    </style>
</head>
<body>
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="col-12 d-flex align-items-center justify-content-center" style="height:10%; background-color: #1e4b25;">
                <p class="pt-3 text-white" style="font-size:50px; font-family: niramit-bold;">
                    สถานะผู้ป่วยในแผนกฉุกเฉิน แยกตามโซนสี
                </p>
            </div>

            <div class="col border border-danger mr-1" style="height:90%;border-width:5px !important;">
                <div class="row">
                    <div class="col-6 bg-danger">
                        <h3 class="mt-2 text-white" style="font-size:34px; font-family: niramit-bold;">ผู้ป่วยฉุกเฉินวิกฤต</h3>
                    </div>
                    <div class="col-6 bg-danger d-flex justify-content-end">
                        <h3 class="mt-2 text-white" style="font-size:34px; font-family: niramit-bold;">3 ราย</h3>
                    </div>
                    <div class="col-12">
                        <table class="mt-2 table table-striped table-sm">
                            <thead class="bg-dark text-white" style="font-size:24px;">
                                <tr>
                                    <th scope="col">เตียง</th>
                                    <th scope="col">HN</th>
                                    <th scope="col">วัน (เวลา)</th>
                                    <th scope="col">สถานะ</th>
                                </tr>
                            </thead>
                            <tbody style="font-size:32px;">
                                <tr>
                                    <th scope="row">01</th>
                                    <td>Test1</td>
                                    <td>Test1</td>
                                    <td>Test1</td>
                                </tr>
                                <tr>
                                    <th scope="row">02</th>
                                    <td>9999999</td>
                                    <td>00-AAA 00:00</td>
                                    <td>รอตรวจ</td>
                                </tr>
                                <tr>
                                    <th scope="row">03</th>
                                    <td>Test3</td>
                                    <td>Test3</td>
                                    <td>Test3</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col border border-warning mr-1" style="height:90%;border-width:5px !important;">
                <div class="row">
                    <div class="col-6 bg-warning">
                        <h3 class="mt-2 text-white" style="font-size:34px; font-family: niramit-bold;">ผู้ป่วยฉุกเฉินไม่รุนแรง</h3>
                    </div>
                    <div class="col-6 bg-warning d-flex justify-content-end">
                        <h3 class="mt-2 text-white" style="font-size:34px; font-family: niramit-bold;">3 ราย</h3>
                    </div>
                    <div class="col-12">
                        <table class="mt-2 table table-striped table-sm">
                            <thead class="bg-dark text-white" style="font-size:24px;">
                                <tr>
                                    <th scope="col">เตียง</th>
                                    <th scope="col">HN</th>
                                    <th scope="col">วัน (เวลา)</th>
                                    <th scope="col">สถานะ</th>
                                </tr>
                            </thead>
                            <tbody style="font-size:32px;">
                                <tr>
                                    <th scope="row">01</th>
                                    <td>Test1</td>
                                    <td>Test1</td>
                                    <td>Test1</td>
                                </tr>
                                <tr>
                                    <th scope="row">02</th>
                                    <td>9999999</td>
                                    <td>00-AAA 00:00</td>
                                    <td>รอตรวจ</td>
                                </tr>
                                <tr>
                                    <th scope="row">03</th>
                                    <td>Test3</td>
                                    <td>Test3</td>
                                    <td>Test3</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col border border-success mr-1" style="height:90%;border-width:5px !important;">
                <div class="row">
                    <div class="col-6 bg-success">
                        <h3 class="mt-2 text-white" style="font-size:34px; font-family: niramit-bold;">ผู้ป่วยทั่วไป</h3>
                    </div>
                    <div class="col-6 bg-success d-flex justify-content-end">
                        <h3 class="mt-2 text-white" style="font-size:34px; font-family: niramit-bold;">3 ราย</h3>
                    </div>

                    <div class="col-12">
                        <table class="mt-2 table table-striped table-sm">
                            <thead class="bg-dark text-white" style="font-size:24px;">
                                <tr>
                                    <th scope="col">เตียง</th>
                                    <th scope="col">HN</th>
                                    <th scope="col">วัน (เวลา)</th>
                                    <th scope="col">สถานะ</th>
                                </tr>
                            </thead>
                            <tbody style="font-size:32px;">
                                <tr>
                                    <th scope="row">01</th>
                                    <td>Test1</td>
                                    <td>Test1</td>
                                    <td>Test1</td>
                                </tr>
                                <tr>
                                    <th scope="row">02</th>
                                    <td>9999999</td>
                                    <td>00-AAA 00:00</td>
                                    <td>รอตรวจ</td>
                                </tr>
                                <tr>
                                    <th scope="row">03</th>
                                    <td>Test3</td>
                                    <td>Test3</td>
                                    <td>Test3</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="col-2 border border-primary" style="height:90%; border-width:5px !important;">
                <div class="row">
                    <div class="col-12 bg-primary">
                        <h3 class="mt-2 text-white" style="font-size:34px; font-family: niramit-bold;">ผู้ป่วยรอตรวจ</h3>
                    </div>
                    <div class="col-12">
                        <table id="waitingTable" class="table table-striped table-sm">
                            <thead class="bg-dark text-white" style="font-size:24px;">
                                <tr>
                                <th scope="col">ลำดับ</th>
                                <th scope="col">HN</th>
                                <th scope="col">วัน (เวลา)</th>
                                </tr>
                            </thead>
                            <tbody id="waitingDataBody" style="font-size:26px;"> 
                                <?php 
                                    $data = $this->getAllERRegist;
                                    for ($i=0; $i < count($data); $i++) {
                                        $date = new DateTime($data[$i]["DATE_Q"]);
                                        $dateformat = date_format($date ,"d-M H:i");
                                        echo "<tr>";
                                            echo "<th scope='row'>" . ($i+1) . "</th>";
                                            echo "<td>" . $data[$i]["HN"] . "</td>";
                                            echo "<td>" . $dateformat . "</td>";
                                        echo "</tr>";
                                    }
                                ?>                  
                            </tbody>
                        </table>

                        <!-- <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php 
                                    $data = $this->getAllERRegist;
                                    $pageCount = count($data) / 15;
                                    echo $pageCount;
                                ?>      
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="..." alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="..." alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="..." alt="Third slide">
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="bower_components/tether/dist/js/tether.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/datatables/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>

    <script>
        // $('#carouselExampleSlidesOnly').carousel()
        // $('#carouselExampleSlidesOnly').carousel({
        //     interval: 5000
        // })

        var table = $('#waitingTable').DataTable({
            "lengthChange": false,
            "ordering": false,
            "searching": false,
            "info":     false,
            "pageLength": 10
        });

        $('#waitingTable').dataTable({
            "destroy": true,
            "bPaginate": false,
            "ordering": false,
            "searching": false,
            "info":     false,
            "dom": 'Bfrtip'
        });
        table.page(0).draw(false);  //Set default page 0

        var info = table.page.info();
        
        setInterval(function(){ 
            // $.ajax({
            //     type:"GET",
            //     url:"http://61.19.201.10/HosApp/rest/v1/?/er_regis/cl_regis/HN",
            //     success: function(data){      
            //         // var element = document.getElementById("waitingDataBody");
            //         // element.innerHTML = "";
            //         $('#waitingTable').DataTable().ajax.reload();
            //         var bodyStr = "";
            //         bodyStr += "<tbody>";
            //         for (let index = 0; index < data.length; index++) {
            //             // $('#waitingDataBody').append(
            //             //     "<tr>"+
            //             //     "<th scope='row'>"+ (index+1) +"</th>"+
            //             //     "<td>" + data[index]["HN"] + "</td>" +
            //             //     "<td>" + data[index]["DATE_Q"] + "</td>"+
            //             //     "</tr>"
            //             // );
            //             bodyStr += "<tr>" + 
            //                             "<td>"+ (index+1) + "</td>"+ 
            //                             "<td>"+ data[index]["HN"] + "</td>"+
            //                         "</tr>";
            //         }
            //         bodyStr += "</tbody>";

            //         $('#waitingTable').append(bodyStr);
            //     }
            // });

            var pageNum = (info.page < info.pages-1) ? info.page + 1 : 0;
            info.page = pageNum;            
            console.log("Page: " + (pageNum+1));
            if (pageNum < info.pages) {
                table.page(pageNum).draw(false);    
            }
        }, 3000); 
        

        setInterval(function(){ 
            location.reload();
        },60000);
    </script>
</body>
</html>
