<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllERRegist()
    {
        $response = file_get_contents('https://his01.psu.ac.th/HosApp/rest/v1/?/er_regis/cl_regis/HN');
        // $response = array(
        //     "name" => "Famz",
        //     "age" => "27"
        // );
        
        $jsonData = json_decode($response,true );
        return $jsonData;
    }
}
