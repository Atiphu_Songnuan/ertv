<?php
class Main extends Controller
{
    public function __construct()
    {
        parent::__construct('main');
        $this->views->getAllERRegist = $this->model->getAllERRegist();
    }

    public function index()
    {
        $this->views->render('main/index');
    }

}
