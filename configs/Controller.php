<?php
class Controller
{
    public function __construct($name = false)
    {
        // echo 'Inside main controller.<br/>';
        $this->views = new View();
        $this->loadModel($name);
    }

    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }
}
